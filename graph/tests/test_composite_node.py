# Testing composite node classes and functions.

from unittest import TestCase
from typing import Dict, List, Set

from graph.core.data_node import DataNode
from graph.core.function_node import FunctionNode
from graph.core.composite_node import CompositeNode

from graph.tests.test_utils import check_outputs


class CompositeNodeTesting(TestCase):
    def test_base_composite_node(self):
        # Build composite node as composition of a data node and a function depending on it.
        underlying_data: Dict[str, int] = {'x': 3, 'y': 2, 'z': 3}
        data_node = DataNode('data', underlying_data)
        output_name: str = 'product_x_y'
        fun_node = FunctionNode('fun', (lambda x, y: x * y), {data_node}, output_name=output_name)

        underlying_nodes = {data_node, fun_node}
        composite_node = CompositeNode('composite', underlying_nodes)

        # Check that the underlying nodes have been recorded.
        self.assertTrue(underlying_nodes == composite_node.underlying_nodes)

        # Check dependencies as union of the underlying node dependencies.
        self.assertTrue(composite_node.dependencies == data_node.dependencies.union(fun_node.dependencies))

        # Check that the potential outputs are the union of the underlying node outputs.
        self.assertTrue(composite_node.potential_outputs ==
                        data_node.potential_outputs.union(fun_node.potential_outputs))

        # Check cache is empty as the populated caches will be for the underlying nodes.
        self.assertTrue(len(composite_node.cache) == 0)

        # Check that all the outputs can be produced and match the outputs produced by the underlying nodes.
        produced_outputs, missing_outputs = composite_node.outputs(composite_node.potential_outputs)
        self.assertTrue(len(missing_outputs) == 0)
        for node in composite_node.underlying_nodes:
            for output in node.potential_outputs:
                current_produced_outputs, current_missing_outputs = node.outputs([output])
                self.assertTrue(produced_outputs[output] == current_produced_outputs[output])

        # Check cache is still empty after requesting the outputs.
        self.assertTrue(len(composite_node.cache) == 0)
