# Testing function node classes and functions.

from unittest import TestCase
from typing import Dict, List, Set

from graph.core.data_node import DataNode
from graph.core.function_node import FunctionNode, produce_output_names

from graph.tests.test_utils import check_outputs


class FunctionNodeTesting(TestCase):
    def test_base_function_node(self):
        def test_fun(x, y):
            return x * y

        underlying_data: Dict[str, int] = {'x': 3, 'y': 2, 'z': 3}
        data_node = DataNode('data', underlying_data)
        output_name: str = 'product_x_y'
        fun_node = FunctionNode('fun', test_fun, [data_node], output_name=output_name)

        # Check potential outputs.
        potential_outputs: List[str] = fun_node.potential_outputs
        self.assertTrue(len(potential_outputs) == 1)
        self.assertTrue(output_name in potential_outputs)

        # Check function arguments.
        expected_function_args: Set[str] = {'x', 'y'}
        self.assertTrue(expected_function_args == fun_node.underlying_function_args)

        # Check default function args.
        self.assertTrue(len(fun_node.underlying_function_default_args) == 0)

        # Check renamed arguments.
        # 1 entry per potential output and each entry should be empty.
        self.assertTrue(len(fun_node.renamed_arguments) == len(fun_node.potential_outputs))
        self.assertTrue(len(fun_node.renamed_arguments[output_name]) == 0)

        # Check no defined default arguments.
        # 1 entry per potential output and each entry should be empty.
        self.assertTrue(len(fun_node.defined_default_arguments) == len(fun_node.potential_outputs))
        self.assertTrue(len(fun_node.defined_default_arguments[output_name]) == 0)

        # Check empty cache.
        self.assertTrue(len(fun_node.cache) == 0)

        # Check that fetched inputs are the same as the underlying data.
        expected_inputs: Dict[str, int] = \
            {key: val for key, val in underlying_data.items() if key in fun_node.underlying_function_args}
        self.assertTrue(fun_node.get_inputs(output_name) == expected_inputs)

        # Check node outputs.
        data_outputs, missing_outputs = fun_node.outputs([output_name])
        self.assertTrue(len(missing_outputs) == 0)
        self.assertTrue(data_outputs[output_name] == test_fun(**expected_inputs))

        # Assert that the single output call returns the same as multi output call.
        self.assertTrue(data_outputs[output_name] == fun_node.output(output_name))

        # Check node cache.
        # Assert that there only is the result just computed.
        self.assertTrue(len(fun_node.cache) == 1)
        # Fetch the cached element with the cache key function of the output name and the inputs.
        cache_key: str = fun_node.get_cache_key(output_name, expected_inputs)
        self.assertTrue(data_outputs[output_name] == fun_node.cache[cache_key])

    def test_produce_output_names(self):
        # Check simple case: no renamed inputs and default input values.
        base_output_name: str = 'fun'
        function_args: List[str] = ['x', 'y', 'z']
        output_names, renamed_inputs, default_input_values = \
            produce_output_names(base_output_name, function_args, None, None)

        # Assert that there is only 1 output equal to base output name with no renamed inputs and default input values.
        self.assertTrue(len(output_names) == 1)
        self.assertTrue(base_output_name in output_names)
        for d in (renamed_inputs, default_input_values):
            self.assertTrue(len(d) == len(output_names))
            self.assertTrue(len(d[base_output_name]) == 0)

        # Check case with renamed arguments only.
        renamed_arguments: Dict[str, List[str]] = {'x': ['foo'], 'y': ['dummy', 'test']}
        output_names, renamed_inputs, default_input_values = \
            produce_output_names(base_output_name, function_args, renamed_arguments, None)
        expected_renamed_inputs: Dict[str, Dict[str, str]] = \
            {'fun_foo_dummy': {'x': 'foo', 'y': 'dummy'}, 'fun_foo_test': {'x': 'foo', 'y': 'test'}}
        expected_output_names = set(expected_renamed_inputs.keys())
        expected_default_input_values = {key: {} for key in expected_output_names}
        check_outputs(self,
                      (output_names, renamed_inputs, default_input_values),
                      (expected_output_names, expected_renamed_inputs, expected_default_input_values))

        # Check case with default argument values only.
        default_argument_values: Dict[str, List['ValueType']] = {'x': [1], 'y': [2, 3], 'z': [[1, 2]]}
        output_names, renamed_inputs, default_input_values = \
            produce_output_names(base_output_name, function_args, None, default_argument_values)
        expected_default_input_values: Dict[str, Dict[str, 'ValueType']] = \
            {'fun_x1_y2_z[1, 2]': {'x': 1, 'y': 2, 'z': [1, 2]},
             'fun_x1_y3_z[1, 2]': {'x': 1, 'y': 3, 'z': [1, 2]}}
        expected_output_names = set(expected_default_input_values.keys())
        expected_renamed_inputs = {key: {} for key in expected_output_names}
        check_outputs(self,
                      (output_names, renamed_inputs, default_input_values),
                      (expected_output_names, expected_renamed_inputs, expected_default_input_values))

        # Check case with both renamed arguments and default argument values.
        default_argument_values: Dict[str, List['ValueType']] = {'foo': [1], 'dummy': [2, 3], 'z': [[1, 2]]}
        output_names, renamed_inputs, default_input_values = \
            produce_output_names(base_output_name, function_args, renamed_arguments, default_argument_values)
        expected_default_input_values = \
            {'fun_foo1_dummy2_z[1, 2]': {'foo': 1, 'dummy': 2, 'z': [1, 2]},
             'fun_foo1_dummy3_z[1, 2]': {'foo': 1, 'dummy': 3, 'z': [1, 2]},
             'fun_foo1_test_z[1, 2]': {'foo': 1, 'z': [1, 2]}}
        expected_renamed_inputs = \
            {'fun_foo1_dummy2_z[1, 2]': {'x': 'foo', 'y': 'dummy'},
             'fun_foo1_dummy3_z[1, 2]': {'x': 'foo', 'y': 'dummy'},
             'fun_foo1_test_z[1, 2]': {'x': 'foo', 'y': 'test'}}
        expected_output_names = set(expected_default_input_values.keys())
        check_outputs(self,
                      (output_names, renamed_inputs, default_input_values),
                      (expected_output_names, expected_renamed_inputs, expected_default_input_values))

    def test_complex_function_node(self):
        def test_fun(v, w, x, y, z=3):
            return v + w + x + y + z

        underlying_data: Dict[str, int] = {'v': 3, 'foo': 2}
        data_node = DataNode('data', underlying_data)
        output_name: str = 'sum'
        fun_node = FunctionNode('fun', test_fun, [data_node],
                                renamed_arguments={'w': ['foo'], 'x': ['x_1', 'x_2']},
                                output_name=output_name,
                                default_argument_values={'x_1': [1], 'x_2': [2], 'y': [1, 2]})

        # Check potential outputs.
        expected_potential_outputs: Set[str] = \
            {'sum_foo_x_11_y1', 'sum_foo_x_11_y2', 'sum_foo_x_22_y1', 'sum_foo_x_22_y2'}
        self.assertTrue(fun_node.potential_outputs == expected_potential_outputs)

        # Check function arguments.
        expected_function_args: Set[str] = {'v', 'w', 'x', 'y', 'z'}
        self.assertTrue(expected_function_args == fun_node.underlying_function_args)

        # Check default function args.
        expected_default_args: Dict[str, 'ValueType'] = {'z': 3}
        self.assertTrue(expected_default_args == fun_node.underlying_function_default_args)

        # Check renamed arguments.
        # Each potential output has an associated set of renamed argument.
        expected_renamed_arguments: Dict[str, Dict[str, str]] = \
            {'sum_foo_x_11_y1': {'w': 'foo', 'x': 'x_1'},
             'sum_foo_x_11_y2': {'w': 'foo', 'x': 'x_1'},
             'sum_foo_x_22_y1': {'w': 'foo', 'x': 'x_2'},
             'sum_foo_x_22_y2': {'w': 'foo', 'x': 'x_2'}}
        self.assertTrue(len(fun_node.renamed_arguments) == len(fun_node.potential_outputs))
        self.assertTrue(fun_node.renamed_arguments == expected_renamed_arguments)

        # Check no defined default arguments.
        # Each potential output has an associated set of defined default argument.
        expected_defined_default_args: Dict[str, Dict[str, 'ValueType']] = \
            {'sum_foo_x_11_y1': {'x_1': 1, 'y': 1},
             'sum_foo_x_11_y2': {'x_1': 1, 'y': 2},
             'sum_foo_x_22_y1': {'x_2': 2, 'y': 1},
             'sum_foo_x_22_y2': {'x_2': 2, 'y': 2}}
        self.assertTrue(len(fun_node.defined_default_arguments) == len(fun_node.potential_outputs))
        self.assertTrue(fun_node.defined_default_arguments == expected_defined_default_args)

        # Check empty cache.
        self.assertTrue(len(fun_node.cache) == 0)

        # Check fetched inputs and outputs.
        expected_inputs: Dict[str, Dict[str, 'ValueType']] = \
            {'sum_foo_x_11_y1': {'x': 1, 'y': 1, 'z': 3, 'w': 2, 'v': 3},
             'sum_foo_x_11_y2': {'x': 1, 'y': 2, 'z': 3, 'w': 2, 'v': 3},
             'sum_foo_x_22_y1': {'x': 2, 'y': 1, 'z': 3, 'w': 2, 'v': 3},
             'sum_foo_x_22_y2': {'x': 2, 'y': 2, 'z': 3, 'w': 2, 'v': 3}}
        for output in expected_potential_outputs:
            expected_output: 'ValueType' = test_fun(**expected_inputs[output])
            current_expected_inputs: Dict[str, 'ValueType'] = expected_inputs[output]
            cache_key: str = fun_node.get_cache_key(output, current_expected_inputs)
            self.assertTrue(fun_node.get_inputs(output) == current_expected_inputs)
            self.assertTrue(fun_node.output(output) == expected_output)
            self.assertTrue(fun_node.cache[cache_key] == expected_output)

        # Assert that the single output call returns the same as multi output call.
        expected_outputs: Dict[str, 'ValueType'] = \
            {output: fun_node.output(output) for output in fun_node.potential_outputs}
        produced_outputs, missing_outputs = fun_node.outputs(fun_node.potential_outputs)
        self.assertTrue(expected_outputs == produced_outputs)
        self.assertTrue(len(missing_outputs) == 0)

        # Check node cache.
        # Assert that the results for all the potential outputs have been cached.
        self.assertTrue(len(fun_node.cache) == len(fun_node.potential_outputs))
