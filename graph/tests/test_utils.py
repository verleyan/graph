# Helper functions for tests.

from typing import Iterable
from unittest import TestCase


def check_outputs(test_case: TestCase,
                  produced_outputs: Iterable['ValueType'],
                  expected_outputs: Iterable['ValueType']):
    for produced_output, expected_output in zip(produced_outputs, expected_outputs):
        test_case.assertTrue(produced_output == expected_output)
