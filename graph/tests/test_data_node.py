# Testing data node classes and functions.

from unittest import TestCase
from typing import Dict, List
from functools import partial
import pandas as pd

from graph.core.data_node import DataNode, IndexedDataNode


class DataNodeTesting(TestCase):
    def test_base_data_node_outputs(self):
        # Creating data nodes.
        underlying_data: Dict[str, int] = {'x': 4, 'y': 2, 'z': 3}
        node_name: str = 'data'
        data_node = DataNode(node_name, underlying_data)

        # Checking data node outputs.
        data_outputs, missing_outputs = data_node.outputs(list(underlying_data.keys()))

        # Assert that all the data has been fetched.
        self.assertTrue(len(missing_outputs) == 0)
        for key, val in underlying_data.items():
            self.assertTrue(data_outputs[key] == val)

        # Assert that when unavailable data is requested, the names are part of the missing outputs.
        extra_data_keys: List[str] = ['a', 'b']
        data_outputs, missing_outputs = data_node.outputs(extra_data_keys)
        self.assertTrue(len(data_outputs) == 0)
        self.assertTrue(len(set(missing_outputs).difference(extra_data_keys)) == 0)

    def test_base_data_node_merge(self):
        # Creating data nodes.
        underlying_data_1: Dict[str, int] = {'a': 1, 'b': 2}
        underlying_data_2: Dict[str, str] = {'c': 'foo', 'd': 'dummy'}
        data_node_1 = DataNode('data_1', underlying_data_1)
        data_node_2 = DataNode('data_2', underlying_data_2)

        merged_data_node: DataNode = DataNode.merge('data_merged', (data_node_1, data_node_2))

        # Assert that the merged data node is also a DataNode.
        self.assertTrue(isinstance(merged_data_node, DataNode))

        # Checking merged data node outputs.
        expected_merged_data: Dict[str, 'ValueType'] = {}
        expected_merged_data.update(underlying_data_1)
        expected_merged_data.update(underlying_data_2)
        data_outputs, missing_outputs = merged_data_node.outputs(list(expected_merged_data.keys()))

        # Assert that all the data has been fetched.
        self.assertTrue(len(missing_outputs) == 0)
        for key, val in expected_merged_data.items():
            self.assertTrue(data_outputs[key] == val)

        # Assert that an exception is raised if non data nodes are merged together with DataNode.merge
        self.assertRaises(Exception,
                          partial(DataNode.merge, name='data_merged_failure'),
                          data_nodes=(data_node_1, data_node_2.cache))

    def test_index_data_node(self):
        underlying_data_1 = {'a': pd.Series({'foo': 1, 'dummy': 2}), 'b': pd.Series({'foo': 1, 'dummy': 2})}
        underlying_data_2 = {'c': pd.DataFrame([[1, 2, 3], [4, 5, 6], [7, 8, 9]], index=['foo', 'dummy', 'test'])}
        indexed_data_node_1 = IndexedDataNode('data_1', underlying_data_1)
        indexed_data_node_2 = IndexedDataNode('data_2', underlying_data_2)
        merged_indexed_data_node: IndexedDataNode = \
            IndexedDataNode.merge('merged_data', (indexed_data_node_1, indexed_data_node_2))

        # Check indexed data node outputs without specifying index.
        expected_underlying_data: Dict[str, 'IndexDataType'] = {}
        expected_underlying_data.update(underlying_data_1)
        expected_underlying_data.update(underlying_data_2)
        data_outputs, missing_outputs = merged_indexed_data_node.outputs(list(expected_underlying_data.keys()))

        # Assert that all the data has been fetched.
        self.assertTrue(len(missing_outputs) == 0)
        for key, val in expected_underlying_data.items():
            self.assertTrue(data_outputs[key].equals(val))

        # Check indexed data node outputs while specifying index.
        # only fetch for index == 'foo'.
        index: str = 'foo'
        data_outputs, missing_outputs = merged_indexed_data_node.outputs(list(expected_underlying_data.keys()), index)
        self.assertTrue(len(missing_outputs) == 0)
        for key, val in expected_underlying_data.items():
            rhs_val = val.loc[index]
            lhs_val = data_outputs[key]
            if isinstance(rhs_val, (pd.Series, pd.DataFrame)):
                self.assertTrue(lhs_val.equals(rhs_val))
            else:
                self.assertTrue(lhs_val == rhs_val)

        # Fetch different indices for the different outputs.
        # If no indices are specified for one output, then all the data for this output is fetched.
        indices: Dict[str, List[str]] = {'a': ['foo'], 'c': ['test', 'dummy']}
        data_outputs, missing_outputs = \
            merged_indexed_data_node.outputs(list(expected_underlying_data.keys()), indices)
        self.assertTrue(len(missing_outputs) == 0)
        for key, val in expected_underlying_data.items():
            rhs_val = val.loc[indices[key]] if key in indices else val
            lhs_val = data_outputs[key]
            if isinstance(rhs_val, (pd.Series, pd.DataFrame)):
                self.assertTrue(lhs_val.equals(rhs_val))
            else:
                self.assertTrue(lhs_val == rhs_val)
