# Helper functions to check types.

def check_dict_value_type(d: dict, expected_type):
    for val in d.values():
        if not isinstance(val, expected_type):
            raise Exception(f"{str(d)} must have {str(expected_type)} as values.")
