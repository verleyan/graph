# Helper functions for collections.

from typing import Iterable


def union_iterable_sets(iterable_sets: Iterable[set]) -> set:
    merged_set: set = set()
    return merged_set.union(*iterable_sets)
