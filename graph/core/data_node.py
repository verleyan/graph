# Classes and helper functions for Data Nodes.

from typing import Dict, Tuple, Iterable, List
import pandas as pd


from graph.core.node import Node


class DataNode(Node):
    def __init__(self, name: str, underlying_data: Dict[str, 'ValueType']):
        super().__init__(name)
        self.cache = underlying_data
        self.potential_outputs = set(underlying_data.keys())

    @staticmethod
    def merge(name: str, data_nodes: Iterable['DataNode']) -> 'DateNode':
        merged_underlying_data: Dict[str, 'ValueType'] = {}
        for data_node in data_nodes:
            if not isinstance(data_node, DataNode):
                raise Exception(f'Element {str(data_node)} is of unexpected type {str(type(data_node))}.')
            merged_underlying_data.update(data_node.cache)
        return DataNode(name, merged_underlying_data)

    def outputs(self, output_names: Iterable[str]) -> Tuple[Dict[str, 'ValueType'], List[str]]:
        return self.fetch_from_cache(output_names)


class IndexedDataNode(DataNode):
    def __init__(self, name: str, underlying_data: Dict[str, 'IndexDataType']):
        for key, val in underlying_data.items():
            if not isinstance(val, (pd.Series, pd.DataFrame)):
                raise Exception(f'{key}: Expected pandas Series or DataFrame but got {str(type(val))}.')
        super().__init__(name, underlying_data)

    @staticmethod
    def merge(name: str, indexed_data_nodes: Iterable['IndexedDataNode']) -> 'IndexedDataNode':
        merged_underlying_data: Dict[str, 'ValueType'] = {}
        for indexed_data_node in indexed_data_nodes:
            if not isinstance(indexed_data_node, IndexedDataNode):
                raise Exception(f'Node {indexed_data_node.name} is of unexpected type {str(type(indexed_data_node))}.')
            merged_underlying_data.update(indexed_data_node.cache)
        return IndexedDataNode(name, merged_underlying_data)

    def outputs(self, output_names: Iterable[str],
                index_val: ['IndexType', Dict[str, Iterable['IndexType']], None] = None) \
            -> Tuple[Dict[str, 'ValueType'], List[str]]:
        fetched_outputs, missing_outputs = super().outputs(output_names)
        if index_val is None:
            pass
        elif isinstance(index_val, dict):
            for key, index in index_val.items():
                fetched_outputs[key] = fetched_outputs[key].loc[index]
            return fetched_outputs, missing_outputs
        else:
            for key, val in fetched_outputs.items():
                fetched_outputs[key] = val.loc[index_val]
        return fetched_outputs, missing_outputs
