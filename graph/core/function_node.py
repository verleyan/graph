# Classes and helper functions for Function Nodes.

from typing import List, Dict, Tuple, Iterable, Set
import inspect
import itertools

from graph.core.node import Node
from graph.utils.type_check import check_dict_value_type


def get_function_arg_names(fun: 'Function') -> Set[str]:
    fun_signature: inspect.Signature = inspect.signature(fun)
    return set(dict(fun_signature.parameters).keys())


def get_function_default_args(fun: 'Function') -> Dict[str, 'ValueType']:
    fun_signature: inspect.Signature = inspect.signature(fun)
    return {key: val.default for key, val in fun_signature.parameters.items() if val.default != inspect._empty}


def produce_output_names(base_output_name: str,
                         original_args: List[str],
                         renamed_arguments: [Dict[str, List[str]], None],
                         default_argument_values: [Dict[str, List['ValueType']], None]) \
        -> Tuple[Set[str], Dict[str, Dict[str, str]], Dict[str, Dict[str, 'ValueType']]]:
    if (renamed_arguments is None) and (default_argument_values is None):
        return {base_output_name}, {base_output_name: {}}, {base_output_name: {}}
    else:
        # Assert that the values are of type list.
        if renamed_arguments is not None:
            check_dict_value_type(renamed_arguments, list)
        if default_argument_values is not None:
            check_dict_value_type(default_argument_values, list)

        # Initialize the sets.
        output_names: List[str] = []
        renamed_inputs: Dict[str, Dict[str, str]] = {}
        default_input_values: Dict[str, Dict[str, 'ValueType']] = {}

        # Compute all the combinations of the different renamed arguments and default arguments.
        renamed_argument_combinations = \
            list(itertools.product(*renamed_arguments.values())) if renamed_arguments is not None else [None]
        renamed_argument_keys = renamed_arguments.keys() if renamed_arguments is not None else None
        default_argument_combinations = \
            list(itertools.product(*default_argument_values.values())) if default_argument_values is not None else [None]
        default_argument_keys = default_argument_values.keys() if default_argument_values is not None else None

        # Iterate over the combinations of renamed arguments.
        for renamed_argument_combination in renamed_argument_combinations:
            # List renamed inputs.
            if renamed_argument_combination is not None:
                current_renamed_inputs: Dict[str, str] = \
                    {key: val for key, val in zip(renamed_argument_keys, renamed_argument_combination)
                     if key in original_args}
                current_new_input_names: List[str] = [str(v) for v in current_renamed_inputs.values()]
            else:
                current_renamed_inputs: Dict[str, str] = {}
                current_new_input_names: List[str] = []

            # Keep track of the arguments for the current output.
            current_args = [current_renamed_inputs.get(arg, arg) for arg in original_args]

            # Iterate over the combinations of default arguments.
            for default_argument_combination in default_argument_combinations:
                # Prepare output name as a combination of the new input names and specified defaults values.
                output_name: Dict[str, str] = {key: '' for key in current_new_input_names}

                # Add the default values to the output name if specified.
                default_inputs: Dict[str, 'ValueType'] = {}
                if default_argument_combination is not None:
                    for key, val in zip(default_argument_keys, default_argument_combination):
                        # Only add the default value if the associated arg name is among the function arguments.
                        if key in current_args:
                            output_name[key] = str(val)
                            default_inputs[key] = val

                # output name is of the form <base_output_name>_<renamed_arg1>_<default_arg1_name><default_arg1_value>
                output_name_str: str = f'{base_output_name}_' + '_'.join([key+val for key, val in output_name.items()])

                # Go to next iteration if output has already been recorded.
                if output_name_str in output_names:
                    continue

                output_names.append(output_name_str)

                # Record renamed inputs for current output.
                renamed_inputs[output_name_str] = current_renamed_inputs

                # Record default values for current output.
                default_input_values[output_name_str] = default_inputs

        return set(output_names), renamed_inputs, default_input_values


class FunctionNode(Node):
    def __init__(self, name: str,
                 underlying_function: 'Function',
                 dependencies: [Iterable['Node'], None] = None,
                 renamed_arguments: [Dict[str, Iterable[str]], None] = None,
                 output_name: [str, None] = None,
                 default_argument_values: [Dict[str, Iterable['ValueType']], None] = None):
        super().__init__(name, dependencies)
        output_name = underlying_function.__name__ if output_name is None else output_name
        self.underlying_function: 'Function' = underlying_function
        self.underlying_function_args: Set[str] = get_function_arg_names(underlying_function)
        self.underlying_function_default_args: Dict[str, 'ValueType'] = get_function_default_args(underlying_function)
        self.potential_outputs, self.renamed_arguments, self.defined_default_arguments = \
            produce_output_names(output_name, self.underlying_function_args, renamed_arguments, default_argument_values)

    def __repr__(self) -> str:
        underlying_repr: str = super().__repr__()
        underlying_repr += f'underlying function: {self.underlying_function.__name__}\n'
        underlying_repr += f'function args: {str(self.underlying_function_args)}\n'
        underlying_repr += f'function default args: {str(self.underlying_function_default_args)}\n'
        underlying_repr += f'renamed args: {str(self.renamed_arguments)}\n'
        underlying_repr += f'defined default args: {str(self.defined_default_arguments)}'
        return underlying_repr

    @staticmethod
    def get_cache_key(output_name: str, args: Dict[str, 'ValueType']) -> str:
        return f'{output_name}:{str(sorted(args.items()))}'

    def get_inputs(self, output_name: str) -> Dict[str, 'ValueType']:
        # Get renamed args.
        renamed_arguments: Dict[str, str] = self.renamed_arguments[output_name]

        # Get required input names.
        dependency_input_names: Set[str] = \
            {renamed_arguments.get(name, name) for name in self.underlying_function_args}

        # Get default arg values.
        default_arg_values: Dict[str, 'ValueType'] = self.underlying_function_default_args
        default_arg_values = {renamed_arguments.get(name, name): value for name, value in default_arg_values.items()}
        default_arg_values.update(self.defined_default_arguments.get(output_name, {}))

        # Include the default arguments in the inputs.
        input_values: Dict[str, 'ValueType'] = default_arg_values

        # Fetched the missing inputs from the dependencies.
        fetched_input_values, missing_inputs = self.fetch_from_dependencies(dependency_input_names)
        for existing_input_name in input_values.keys():
            if existing_input_name in missing_inputs:
                missing_inputs.remove(existing_input_name)
        input_values.update(fetched_input_values)

        # Check if there are still missing inputs.
        if len(missing_inputs) > 0:
            raise Exception(f"Missing inputs for FunctionNode {self.name}: {str(missing_inputs)}")

        # Renamed the fetched inputs to match the function's arguments' names.
        for key, val in renamed_arguments.items():
            input_values[key] = input_values.pop(val)

        return input_values

    def output(self, output_name: str) -> 'ValueType':
        inputs: Dict[str, 'ValutType'] = self.get_inputs(output_name)
        cache_key: str = self.get_cache_key(output_name, inputs)
        res: ['ValueType', None] = self.cache.get(cache_key)
        if res is None:
            res = self.underlying_function(**inputs)
            self.cache[cache_key] = res
        return res

    def outputs(self, output_names: List[str]) -> Tuple[Dict[str, 'ValueType'], List[str]]:
        missing_outputs: List[str] = []
        fetched_outputs: Dict[str, 'ValueType'] = {}

        for output_name in output_names:
            if output_name in self.potential_outputs:
                fetched_outputs[output_name] = self.output(output_name)
            else:
                missing_outputs.append(output_name)

        return fetched_outputs, missing_outputs
