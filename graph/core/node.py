# Base class for all nodes.

from typing import Dict, Iterable, Tuple, List, Set


class Node:
    def __init__(self, name: str, dependencies: [Iterable['Node'], None] = None):
        self.name: str = name
        self.dependencies: Set['Node'] = set(dependencies) if dependencies is not None else set()
        self.cache: Dict[str, 'ValueType'] = {}
        self.potential_outputs: Set[str] = set()

    def __repr__(self) -> str:
        attributes = {'dependencies': [dep.name for dep in self.dependencies],
                      'cache': self.cache,
                      'potential_outputs': self.potential_outputs}

        repr_message: str = f'Node {self.name}: \n'
        for key, val in attributes.items():
            repr_message += f'{key}: {str(val)}\n'

        return repr_message

    def fetch_from_dependencies(self, output_names: Set[str]) -> Tuple[Dict[str, 'ValueType'], Set[str]]:
        outputs: Dict[str, 'ValueType'] = {}
        missing_output_names: Set[str] = output_names
        for dependency_node in self.dependencies:
            produced_outputs, missing_output_names = dependency_node.outputs(missing_output_names)
            outputs.update(produced_outputs)
            if len(missing_output_names) == 0:
                break
        return outputs, missing_output_names

    def fetch_from_cache(self, output_names: Set[str]) -> Tuple[Dict[str, 'ValueType'], Set[str]]:
        fetched_outputs: Dict[str, 'ValueType'] = {}
        missing_outputs: Set[str] = set()
        for output_name in output_names:
            output: ['ValueType', None] = self.cache.get(output_name)
            if output is not None:
                fetched_outputs[output_name] = output
            else:
                missing_outputs.add(output_name)
        return fetched_outputs, missing_outputs

    def outputs(self, output_names: Iterable[str]) -> Tuple[Dict[str, 'ValueType'], Iterable[str]]:
        raise Exception(f"Output function not implemented for {str(type(self))}")
