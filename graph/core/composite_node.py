# Class and helper functions for CompositeNode.

from typing import List, Tuple, Dict, Set, Iterable

from graph.core.node import Node
from graph.utils.collection_utils import union_iterable_sets


class CompositeNode(Node):
    def __init__(self, name: str, underlying_nodes: Iterable[Node]):
        super().__init__(name, union_iterable_sets(node.dependencies for node in underlying_nodes))
        self.underlying_nodes: Set[Node] = set(underlying_nodes)
        self.potential_outputs: Set[str] = \
            union_iterable_sets(node.potential_outputs for node in underlying_nodes)

    def __repr__(self) -> str:
        underlying_repr: str = super().__repr__()
        underlying_repr += f'underlying nodes: {str([node.name for node in self.underlying_nodes])}\n'
        return underlying_repr

    def outputs(self, output_names: Set[str]) -> Tuple[Dict[str, 'ValueType'], Set[str]]:
        outputs: Dict[str, 'ValueType'] = {}
        missing_output_names: Set[str] = output_names
        for underlying_node in self.underlying_nodes:
            produced_outputs, missing_output_names = underlying_node.outputs(missing_output_names)
            outputs.update(produced_outputs)
            if len(missing_output_names) == 0:
                break
        return outputs, missing_output_names
